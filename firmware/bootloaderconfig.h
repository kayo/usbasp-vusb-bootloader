/* Name: bootloaderconfig.h
 * Project: USBaspLoader
 * Author: Christian Starkjohann
 * Creation Date: 2007-12-08
 * Tabsize: 4
 * Copyright: (c) 2007 by OBJECTIVE DEVELOPMENT Software GmbH
 * Portions Copyright: (c) 2012 Louis Beaudoin
 * License: GNU GPL v2 (see License.txt)
 * This Revision: $Id: bootloaderconfig.h 729 2009-03-20 09:03:58Z cs $
 */

#ifndef __bootloaderconfig_h_included__
#define __bootloaderconfig_h_included__

#ifndef BOOTLOADER_ADDRESS
#define BOOTLOADER_ADDRESS 0
#endif


/*
General Description:
This file (together with some settings in Makefile) configures the boot loader
according to the hardware.

This file contains (besides the hardware configuration normally found in
usbconfig.h) two functions or macros: bootLoaderInit() and
bootLoaderCondition(). Whether you implement them as macros or as static
inline functions is up to you, decide based on code size and convenience.

bootLoaderInit() is called as one of the first actions after reset. It should
be a minimum initialization of the hardware so that the boot loader condition
can be read. This will usually consist of activating a pull-up resistor for an
external jumper which selects boot loader mode.

bootLoaderCondition() is called immediately after initialization and in each
main loop iteration. If it returns TRUE, the boot loader will be active. If it
returns FALSE, the boot loader jumps to address 0 (the loaded application)
immediately.

For compatibility with Thomas Fischl's avrusbboot, we also support the macro
names BOOTLOADER_INIT and BOOTLOADER_CONDITION for this functionality. If
these macros are defined, the boot loader uses them.
*/

/* ---------------------------- Hardware Config ---------------------------- */

/*#define USB_CFG_IOPORTNAME      B*/
/* This is the port where the USB bus is connected. When you configure it to
 * "B", the registers PORTB, PINB and DDRB will be used.
 */

#define USB_CFG_CLOCK_KHZ       (F_CPU/1000)
/* Clock rate of the AVR in MHz. Legal values are 12000, 16000 or 16500.
 * The 16.5 MHz version of the code requires no crystal, it tolerates +/- 1%
 * deviation from the nominal frequency. All other rates require a precision
 * of 2000 ppm and thus a crystal!
 * Default if not specified: 12 MHz
 */

/* ----------------------- Optional Hardware Config ------------------------ */

/* #define USB_CFG_PULLUP_IOPORTNAME   D */
/* If you connect the 1.5k pullup resistor from D- to a port pin instead of
 * V+, you can connect and disconnect the device from firmware by calling
 * the macros usbDeviceConnect() and usbDeviceDisconnect() (see usbdrv.h).
 * This constant defines the port on which the pullup resistor is connected.
 */
/* #define USB_CFG_PULLUP_BIT          4 */
/* This constant defines the bit number in USB_CFG_PULLUP_IOPORT (defined
 * above) where the 1.5k pullup resistor is connected. See description
 * above for details.
 */

/* ------------------------------------------------------------------------- */
/* ---------------------- feature / code size options ---------------------- */
/* ------------------------------------------------------------------------- */

/*#define HAVE_EEPROM_PAGED_ACCESS    0*/
/* If HAVE_EEPROM_PAGED_ACCESS is defined to 1, page mode access to EEPROM is
 * compiled in. Whether page mode or byte mode access is used by AVRDUDE
 * depends on the target device. Page mode is only used if the device supports
 * it, e.g. for the ATMega88, 168 etc. You can save quite a bit of memory by
 * disabling page mode EEPROM access. Costs ~ 138 bytes.
 */
/*#define HAVE_EEPROM_BYTE_ACCESS     0*/
/* If HAVE_EEPROM_BYTE_ACCESS is defined to 1, byte mode access to EEPROM is
 * compiled in. Byte mode is only used if the device (as identified by its
 * signature) does not support page mode for EEPROM. It is required for
 * accessing the EEPROM on the ATMega8. Costs ~54 bytes.
 */
/*#define BOOTLOADER_CAN_EXIT         1*/
/* If this macro is defined to 1, the boot loader will exit shortly after the
 * programmer closes the connection to the device. Costs ~36 bytes.
 * Required for TINY85MODE
 */
/*#define HAVE_CHIP_ERASE             1*/
/* If this macro is defined to 1, the boot loader implements the Chip Erase
 * ISP command. Otherwise pages are erased on demand before they are written.
 */
/*#define SIGNATURE_BYTES             0x1e, 0x93, 0x0b, 0*/     /* ATtiny85 */
/* This macro defines the signature bytes returned by the emulated USBasp to
 * the programmer software. They should match the actual device at least in
 * memory size and features. If you don't define this, values for ATMega8,
 * ATMega88, ATMega168 and ATMega328 are guessed correctly.
 */

/* The following block guesses feature options so that the resulting code
 * should fit into 2k bytes boot block with the given device and clock rate.
 * Activate by passing "-DUSE_AUTOCONFIG=1" to the compiler.
 * This requires gcc 3.4.6 for small enough code size!
 */
#if USE_AUTOCONFIG
#  undef HAVE_EEPROM_PAGED_ACCESS
#  define HAVE_EEPROM_PAGED_ACCESS     (USB_CFG_CLOCK_KHZ >= 16000)
#  undef HAVE_EEPROM_BYTE_ACCESS
#  define HAVE_EEPROM_BYTE_ACCESS      1
#  undef BOOTLOADER_CAN_EXIT
#  define BOOTLOADER_CAN_EXIT          1
#  undef SIGNATURE_BYTES
#endif /* USE_AUTOCONFIG */

/* ------------------------------------------------------------------------- */

#ifndef MCUCSR          /* compatibility between ATMega8 and ATMega88 */
#  define MCUCSR   MCUSR
#endif

//#define APPCHECKSUM

/* tiny85 Architecture Specifics */
#ifdef __AVR_ATtiny85__
#   define TINY85MODE

#   ifndef APPCHECKSUM
#     define APPCHECKSUM
#  endif

// number of bytes before the boot loader vectors to store the tiny application vector table
#  define TINYVECTOR_RESET_OFFSET     4
#  define TINYVECTOR_USBPLUS_OFFSET   2

#  define RESET_VECTOR_OFFSET         0
#  define USBPLUS_VECTOR_OFFSET       2

// setup interrupt for Pin Change for D+
#  define USB_INTR_CFG            PCMSK
#  define USB_INTR_CFG_SET        (1 << USB_CFG_DPLUS_BIT)
#  define USB_INTR_CFG_CLR        0
#  define USB_INTR_ENABLE         GIMSK
#  define USB_INTR_ENABLE_BIT     PCIE
#  define USB_INTR_PENDING        GIFR
#  define USB_INTR_PENDING_BIT    PCIF
#  define USB_INTR_VECTOR         PCINT0_vect
#endif


/* Application Checksum Section */
#ifdef APPCHECKSUM
/* max 6200ms to not overflow idlePolls variable */
#  define AUTO_EXIT_MS    5000

/* number of bytes before the boot loader vectors to store the application checksum */
#  define APPCHECKSUM_POSITION    6

#  define AUTO_EXIT_CONDITION()   (validApp && !connectedToPc && (idlePolls > (AUTO_EXIT_MS * 10UL)))
#else
#  define AUTO_EXIT_CONDITION()   0
#endif


/* ----------------------- Optional MCU Description ------------------------ */

/* The following configurations have working defaults in usbdrv.h. You
 * usually don't need to set them explicitly. Only if you want to run
 * the driver on a device which is not yet supported or with a compiler
 * which is not fully supported (such as IAR C) or if you use a different
 * interrupt than INT0, you may have to define some of these.
 */
/* #define USB_INTR_CFG            MCUCR */
/* #define USB_INTR_CFG_SET        ((1 << ISC00) | (1 << ISC01)) */
/* #define USB_INTR_CFG_CLR        0 */
/* #define USB_INTR_ENABLE         GIMSK */
/* #define USB_INTR_ENABLE_BIT     INT0 */
/* #define USB_INTR_PENDING        GIFR */
/* #define USB_INTR_PENDING_BIT    INTF0 */
/* #define USB_INTR_VECTOR         INT0_vect */

#ifndef __ASSEMBLER__   /* assembler cannot parse function definitions */

#  ifdef BOOTLOADER_TIMEOUT_BASED
static uint8_t bootLoaderTimeout;
#  endif

static inline void bootLoaderInit(void){

#  ifdef BOOTLOADER_JUMPER_BASED
  /* setup jumper port as input */
#    define _(port,pin,inv) DDR ## port &= ~_BV(pin)
  BOOTLOADER_JUMPER_BASED;
#    undef _
  /* setup pullup on port */
#    define _(port,pin,inv) PORT ## port |= _BV(pin)
  BOOTLOADER_JUMPER_BASED;
#    undef _
#  endif//BOOTLOADER_JUMPER_BASED

#  ifdef BOOTLOADER_TIMEOUT_BASED
#    define _(htimer,prediv,cntval,tickno) bootLoaderTimeout = tickno
  BOOTLOADER_TIMEOUT_BASED;
#    undef _
/* setup timer */
#    define _(htimer,prediv,cntval,tickno)                              \
  TCCR ## htimer = _BV(WGM ## htimer ## 1)         /* CTC mode of operation (Clear Timer on Compare match) */ \
    | _BV(CS ## htimer ## 2) | _BV(CS ## htimer ## 0); /* Clock Select through prescaler CLK/1024 */ \
  /*TCNT ## htimer = 0;*/                 /* Clear Timer Counter */   \
  OCR ## htimer = cntval;      /* Compare value (Clear timer counter after 155 ticks) */
  BOOTLOADER_TIMEOUT_BASED;
#    undef _
#  endif//BOOTLOADER_TIMEOUT_BASED

#  ifndef TINY85MODE
  if(!(MCUCSR & (1 << EXTRF)))    /* If this was not an external reset, ignore */
    leaveBootloader();
  MCUCSR = 0;                     /* clear all reset flags for next time */
#  endif//TINY85MODE
}

#  if defined(BOOTLOADER_UNCONDITIONAL) || defined(BOOTLOADER_CYCLES_BASED)
#    define bootLoaderExit()
#  else//BOOTLOADER_UNCONDITIONAL
static inline void bootLoaderExit(void){

#    ifdef BOOTLOADER_JUMPER_BASED
  /* dismiss pullup on port */
#      define _(port,pin,inv) PORT ## port &= ~_BV(pin)
  BOOTLOADER_JUMPER_BASED;
#      undef _
#    endif//BOOTLOADER_JUMPER_BASED

#    ifdef BOOTLOADER_TIMEOUT_BASED
  TCCR0 = 0;
#    endif//BOOTLOADER_TIMEOUT_BASED

}
#  endif//BOOTLOADER_UNCONDITIONAL

#  ifdef BOOTLOADER_UNCONDITIONAL
#    define bootLoaderCondition() 1
#  endif//BOOTLOADER_UNCONDITIONAL

#  ifdef BOOTLOADER_UNCONDITIONAL
#    define bootLoaderCondition() 1
#  endif//BOOTLOADER_UNCONDITIONAL

#  ifdef BOOTLOADER_JUMPER_BASED
static inline uint8_t bootLoaderCondition(){
  /* check jumper condition */
#    define _(port,pin,inv) return !!(PIN ## port & _BV(pin)) == inv;
  BOOTLOADER_JUMPER_BASED;
#    undef _
}
#  endif//BOOTLOADER_JUMPER_BASED

#  ifdef BOOTLOADER_CYCLES_BASED
#    define bootLoaderCondition() (idlePolls < BOOTLOADER_CYCLES_BASED)
#  endif//BOOTLOADER_CYCLES_BASED

#  ifdef BOOTLOADER_TIMEOUT_BASED
static inline uint8_t bootLoaderCondition(){
#    define _(htimer,prediv,cntval,tickno)      \
  if(TIFR & _BV(OCF ## htimer)){                \
    bootLoaderTimeout--;                        \
    TIFR |= _BV(OCF ## htimer);                 \
  }
  BOOTLOADER_TIMEOUT_BASED;
#    undef _
  return bootLoaderTimeout;
}
#  endif//BOOTLOADER_TIMEOUT_BASED

#endif /* __ASSEMBLER__ */

/* ------------------------------------------------------------------------- */

#endif /* __bootloader_h_included__ */
