cc.opt?=s
ld.map?=on

ifneq ($(device),)
  CFLAGS+=-mmcu=$(device)
  LDFLAGS+=-mmcu=$(device)
else
  $(error Device not specified!)
endif

ifneq ($(clock),)
  clock_hertz=$(call freq-hertz,$(clock))
  def+=F_CPU=$(clock_hertz)
else
  $(error Clock not specified!)
endif

ifneq ($(flash),)
  flash_bytes=$(call size-bytes,$(flash))
  def+=S_FLASH=$(flash_bytes)
endif

CC=avr-gcc
AS=avr-as
SIZE=avr-size
OBJCOPY=avr-objcopy
OBJDUMP=avr-objdump

all: elf bin hex
	$(SIZE) $(project).hex

sim: $(project).bin
	simulavr -d $(device) -c $(clock_hertz) -P simulavr-disp $<

ifeq ($(flasher),avrdude)

FLASHER=avrdude -c $(programer) -p $(device)

prog: $(project).hex
	$(FLASHER) -U flash:w:$<:a
flash: prog

dump: dump.hex
dump.hex:
	$(FLASHER) -U flash:r:$@:h

boot:
	$(FLASHER)

term:
	$(FLASHER) -t
console: term

fuse.n=$(filter-out :%,$(subst :, :,$(1)))
fuse.r=$(addsuffix :r:-:h,$(call fuse.n,$(1)))
fuse.w=$(addsuffix :m,$(subst :,:w:,$(1)))
fuse.op=$(patsubst %,-U %,$(if $(subst w,,$(1)),$(call fuse.r,$(2)),$(call fuse.w,$(2))))

fuse.get:
	@echo Reading fuses..
	@echo $(call fuse.n,$(fuse))
	@echo `$(FLASHER) $(call fuse.op,r,$(fuse)) 2>/dev/null`

fuse.put:
	@echo Flashing fuses..
	$(FLASHER) $(call fuse.op,w,$(fuse))

endif
