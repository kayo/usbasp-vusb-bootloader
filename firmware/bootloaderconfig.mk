def+=SIGNATURE_BYTES=$(devsig)
def+=HAVE_EEPROM_PAGED_ACCESS=$(if $(filter paged,$(eeprom)),1,0)
def+=HAVE_EEPROM_BYTE_ACCESS=$(if $(filter byte,$(eeprom)),1,0)
def+=BOOTLOADER_CAN_EXIT=$(if $(filter-out no,$(canexit)),1,0)
def+=HAVE_CHIP_ERASE=$(if $(filter-out no,$(chiperase)),1,0)

bl.flash?=2K
bl.flash_bytes=$(call size-bytes,$(bl.flash))
bl.start?=$(call calc,$(flash_bytes)-$(bl.flash_bytes))

ifeq ($(variant),unconditional)
  def+=BOOTLOADER_UNCONDITIONAL=1
endif

ifeq ($(variant),jumper-based)
  def+=BOOTLOADER_JUMPER_BASED='$(call parsespec,$(bl.jumper))'
endif

ifeq ($(variant),cycles-based)
  bl.cycles?=$(shell echo '$(bl.timeout)*10000/1' | bc)
  def+=BOOTLOADER_CYCLES_BASED=$(bl.cycles)
endif

ifeq ($(variant),timeout-based)
  bl.htimer?=0    # timer number
  bl.prediv?=1024 # prescaler
  bl.tickhz?=100  # 100 Hz
  bl.cntval=$(shell echo '$(clock_value)/$(bl.prediv)/$(bl.tickhz)-1' | bc)
  bl.tickno=$(shell echo '$(bl.timeout)*$(bl.tickhz)/1' | bc)
  def+=BOOTLOADER_TIMEOUT_BASED='_($(firstword $(bl.htimer)),$(firstword $(bl.prediv)),$(bl.cntval),$(bl.tickno))'
endif

CFLAGS+=$(addprefix -f,no-move-loop-invariants no-tree-scev-cprop no-inline-small-functions)
LDFLAGS+=-Wl$(comachar)--relax$(comachar)--gc-sections $(if $(bl.start),-Wl$(comachar)--section-start=.text=$(call dec2hex,$(bl.start)))
